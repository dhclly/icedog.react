const { Component } = React;

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      leaf: null
    };
  }
  leafToggle = true;
  changeLeaf = () => {
    let stamp = new Date().getMilliseconds();
    if (this.leafToggle) {
      this.toChild = "now leaf is A ,stamp:" + stamp;
      this.setState({
        leaf: <LeafAA toChild={this.toChild} stamp={stamp} />
      });
    } else {
      this.toChild = "now leaf is B ,stamp:" + stamp;
      this.setState({
        leaf: <LeafBB toChild={this.toChild} stamp={stamp} />
      });
    }
    this.leafToggle = !this.leafToggle;
  };
  render() {
    return (
      <div>
        <div>App</div>
        <div>
          <button onClick={() => this.changeLeaf()}>change leaf</button>
        </div>
        <hr />
        <div>Leaf:</div>
        <div style={{ height: "200px", border: "1px solid green" }}>
          {this.state.leaf === null ? "empty" : this.state.leaf}
        </div>
      </div>
    );
  }
}
ReactDOM.render(<App />, document.getElementById("root"));

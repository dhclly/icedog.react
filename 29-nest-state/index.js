const { Component } = React;

class RedBox extends Component {
  render() {
    var {
      texts: { texta, textb, textc }
    } = this.props;
    return (
      <div style={{ border: "1px solid red" }}>
        <div>{texta}</div>
        <div>{textb}</div>
        <div>{textc}</div>
      </div>
    );
  }
}

class App extends Component {
  state = {
    texts: {
      texta: "bb",
      textb: "cc",
      textc: "dd"
    }
  };

  changeTexts = () => {
    this.setState(state => {
      let newState = { ...state };
      newState.texts.texta = "new aaa";
      return newState;
    });
  };
  render() {
    const { texts } = this.state;
    return (
      <div>
        <RedBox texts={texts} />
        <button onClick={() => this.changeTexts()}>change texts</button>
      </div>
    );
  }
}
ReactDOM.render(<App />, document.getElementById("root"));

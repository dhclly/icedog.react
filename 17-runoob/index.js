const { Component } = React;

const Colors = [
  <span key={1} style={{ color: "green" }}>
    Green
  </span>,
  <span key={2} style={{ color: "Blue" }}>
    Blue
  </span>,
  <span key={3} style={{ color: "Red" }}>
    Red
  </span>,
  <span key={4} style={{ color: "Black" }}>
    Black
  </span>
];

const LikeColors = props =>
  props.colors.map((c, i) => (
    <span key={i} style={{ color: c }}>
      {c}
    </span>
  ));

function Greet(props) {
  return <h1>Hi! {props.name}</h1>;
}

function Calc(props) {
  return (
    <h1>
      {props.a}+{props.b}={props.a + props.b}
    </h1>
  );
}

class Welcome extends React.Component {
  render() {
    return <h1>Welcome! {this.props.name}</h1>;
  }
}
//设置组件的props的默认值
Welcome.defaultProps = {
  name: "Unamed"
};

//设置组件的props的值类型
Welcome.propTypes = {
  name: PropTypes.string
};

function SiteName(props) {
  return <p>网站名称：{props.name}</p>;
}

function SiteUrl(props) {
  return (
    <p>
      网站地址：
      <a href={props.url} target="_blank">
        {props.url}
      </a>
    </p>
  );
}

function SiteNickname(props) {
  return <p>网站小名：{props.nickname}</p>;
}

class SiteInfo extends React.Component {
  constructor() {
    super();
    this.state = {
      name: "菜鸟教程",
      site: "https://www.runoob.com",
      nickname: "Runoob"
    };
  }
  render() {
    let style = {
      border: "1px solid black",
      borderRadius: 5,
      width: 400
    };

    return (
      <div style={style}>
        <SiteName name={this.state.name} />
        <SiteUrl url={this.state.site} />
        <SiteNickname nickname={this.state.nickname} />
      </div>
    );
  }
}

class Clock extends Component {
  constructor(props) {
    super(props);
    this.state = { date: new Date() };
  }
  timerID = null;

  componentDidMount() {
    this.timerID = setInterval(() => this.tick(), 1000);
  }

  componentWillUnmount() {
    clearInterval(this.timerID);
  }

  tick() {
    this.setState({
      date: new Date()
    });
  }

  render() {
    let style = {
      marginTop: 10,
      border: "1px solid #6cf",
      borderRadius: 20,
      width: 380,
      padding: 10
    };
    return (
      <div style={style}>
        <h2>现在是 {this.state.date.toLocaleTimeString()}</h2>
      </div>
    );
  }
}

class ToggleButton extends React.Component {
  constructor(props) {
    super(props);
    this.state = { isToggleOn: true };

    // 这边绑定是必要的，这样 `this` 才能在回调函数中使用
    //或者写成箭头函数形式即可。
    //this.handleClick = this.handleClick.bind(this);
  }

  handleClick = () => {
    this.setState(prevState => ({
      isToggleOn: !prevState.isToggleOn
    }));
  };

  render() {
    return (
      <button style={{ width: 100 }} onClick={this.handleClick}>
        {this.state.isToggleOn ? "ON" : "OFF"}
      </button>
    );
  }
}

function HkUserGreeting(props) {
  return <h1>欢迎回来!</h1>;
}

function HkGuestGreeting(props) {
  return <h1>请先注册。</h1>;
}

class HkGreeting extends Component {
  render() {
    const { isLoggedIn } = this.props;
    let Greeting = null;
    if (isLoggedIn) {
      Greeting = <HkUserGreeting />;
    } else {
      Greeting = <HkGuestGreeting />;
    }
    return Greeting;
  }
}

class LoginControl extends React.Component {
  constructor(props) {
    super(props);
    this.handleLoginClick = this.handleLoginClick.bind(this);
    this.handleLogoutClick = this.handleLogoutClick.bind(this);
    this.state = { isLoggedIn: false };
  }

  handleLoginClick() {
    this.setState({ isLoggedIn: true });
  }

  handleLogoutClick() {
    this.setState({ isLoggedIn: false });
  }

  render() {
    const { isLoggedIn } = this.state;

    let button = null;

    if (isLoggedIn) {
      button = <button onClick={this.handleLogoutClick}>注销</button>;
    } else {
      button = <button onClick={this.handleLoginClick}>登录</button>;
    }

    return (
      <div>
        <HkGreeting isLoggedIn={isLoggedIn} />
        {button}
      </div>
    );
  }
}

function Mailbox(props) {
  const unreadMessages = props.unreadMessages;
  return (
    <div>
      <h1>Hello!</h1>
      {unreadMessages.length > 0 && (
        <h2>您有 {unreadMessages.length} 条未读信息。</h2>
      )}
    </div>
  );
}

function WarningBanner(props) {
  if (!props.warn) {
    return null;
  }
  return <div className="warning">警告!</div>;
}

class WarningPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = { showWarning: true };
    this.handleToggleClick = this.handleToggleClick.bind(this);
  }

  handleToggleClick() {
    this.setState(prevState => ({
      showWarning: !prevState.showWarning
    }));
  }

  render() {
    return (
      <div>
        <WarningBanner warn={this.state.showWarning} />
        <button onClick={this.handleToggleClick}>
          {this.state.showWarning ? "隐藏" : "显示"}
        </button>
      </div>
    );
  }
}

class App extends Component {
  render() {
    return (
      <div>
        <div>{Colors}</div>
        <div>
          <LikeColors {...{ colors: ["red", "#66ccff", "pink"] }} />
        </div>
        <Greet {...{ name: "anny" }} />
        <Calc {...{ a: 6, b: 3 }} />
        <Welcome {...{ name: "tom" }} />
        <Welcome />
        {/* 读取默认值 */}
        <SiteInfo />
        <Clock />
        <ToggleButton />
        <LoginControl />
        <Mailbox unreadMessages={["React", "Re: React", "Re:Re: React"]} />
        <WarningPage/>
      </div>
    );
  }
}

ReactDOM.render(<App />, document.getElementById("root"));

1.只有改变 state 对象上的属性，render 才会重新触发,否则其他属性变跟并不会触发 render 属性 

2.父级变更会触发子级的 render

## 生命周期

1. constructor 方法

   方法签名：constructor(props)

   作用：用于组件内部的 state 初始化、事件方法的绑定，

   注意事项：

(1) 方法内的第一句应为 super(props) ，这样可以避免构造方法后面的代码使用 this.props 时出现 undefined 错语

(2) 在构造方法内对 state 应直接进行赋值，而不应调用 setState()方法

(3) 避免拷贝 props 到 state 中，而要用 this.props.xxx , 否则当 props 发生变化时，state 将不能得到及时更新

2. getDerivedStateFromProps方法

    方法签名：static getDerivedStateFromProps(props, state)

    作用：它允许组件根据props的变化而更新其内部state。如果要更新state，该方法必须返回新的state（称之为派生state)，或者返回null代表不更新。这个生命周期方法在render方法执行之前被调用，当组件在mount和updating时都会执行该方法。

     注意事项：

        (1) 在组件的生命周期中，派生state应该谨慎使用，应当在满足当props改变并符合一定的条件时再对state进行更新

　　　　(2) 当组件的setState方法和forceUpdate方法被调用时，也会触发该生命周期方法。

3. shouldComponentUpdate方法

   签名：shouldComponentUpdate(nextProps, nextState)

   作用：该生命周期方法决定当组件的props和state发生改变时，是否需要重新渲染组件。该方法默认情况下返回true，表示需要重新渲染，可以通过返回false阻止组件的向下的生命周期方法的执行。该生命周期方法应该仅作为性能优化的手段去使用，而避免用于其他用途。

   例如当父组件的props发生变化时，我们希望只是重新渲染受影响的一部分节点，如下图：

    

   注意事项：

          （1）当组件的props发生改变或者setState方法执行后会触发该生命周期方法的调用。

          （2） 在组件的mount阶段和forceUpdate()方法执行后，不会触发该生命周期方法调用。

          （3）可以考虑让组件继承React.PureComponent,  其已经实现了React.PureComponent方法，不过它的实现是对当props和state和nextProps和nextState的浅层比较

4. getSnapshotBeforeUpdate方法

   签名：getSnapshotBeforeUpdate(prevProps, prevState)

   作用：该方法在render方法之后，在render的输出到DOM之前执行。在这个生命周期方内允许组件可以从DOM上捕获一些信息（例如 scroll position），该方法返回的任何值都将作为参数（后称快照值）传递给componentDidUpdate()生命周期方法。该方法要么返回一个快照值或者null 。

   示例：这个方法的使用场景，比如一个聊天窗口，当用户发出新的聊天信息后，可能需要根据情况滚动窗口
const { Component } = React;

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      leafAAVisible: false,
      leafBBVisible: false
    };
  }
  toChild = "nothing";
  stamp = null;
  leafToggle = true;
  changeLeaf = () => {
    this.stamp = new Date().getMilliseconds();
    if (this.leafToggle) {
      this.toChild = "now leaf is A ,stamp:" + this.stamp;
      this.setState({
        leafAAVisible: true,
        leafBBVisible: false
      });
    } else {
      this.toChild = "now leaf is B ,stamp:" + this.stamp;
      this.setState({
        leafAAVisible: false,
        leafBBVisible: true
      });
    }
    this.leafToggle = !this.leafToggle;
  };
  render() {
    return (
      <div>
        <div>App</div>
        <div>
          <button onClick={() => this.changeLeaf()}>change leaf</button>
        </div>
        <hr />
        <div>Leaf:</div>
        <div style={{ height: "200px", border: "1px solid green" }}>
          <LeafAA
            toChild={this.toChild}
            stamp={this.stamp}
            visible={this.state.leafAAVisible}
          />
          <LeafBB
            toChild={this.toChild}
            stamp={this.stamp}
            visible={this.state.leafBBVisible}
          />
        </div>
      </div>
    );
  }
}
ReactDOM.render(<App />, document.getElementById("root"));

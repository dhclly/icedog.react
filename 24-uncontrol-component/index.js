const { Component } = React;

class NameForm extends React.Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.name = React.createRef();
    this.age = React.createRef();
  }

  handleSubmit(event) {
    let name = this.name.current.value;
    let age = this.age.current.value;
    alert(`A name was submitted:${name},age:${age}`);
    event.preventDefault();
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <label>
          Name:
          <input type="text" ref={this.name} defaultValue="alice" />
          {/* <input type="text" ref={this.name} value="alice"/> */}
        </label>
        <label>
          Age:
          <input
            type="number"
            max="150"
            min="10"
            ref={this.age}
            defaultValue="18"
          />
        </label>
        <label>
          Remember：
          <input type="checkbox" defaultChecked="true" />
        </label>
        <input type="submit" value="Submit" />
      </form>
    );
  }
}
class FileInput extends React.Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.fileInput = React.createRef();
  }
  handleSubmit(event) {
    event.preventDefault();
    alert(`Selected file - ${[...this.fileInput.current.files].map(f=>f.name).join(",")}`);
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <label>
          Upload file:
          <input type="file" ref={this.fileInput} multiple/>
        </label>
        <br />
        <button type="submit">Submit</button>
      </form>
    );
  }
}
class App extends Component {
  render() {
    return (
      <div>
        <NameForm />
        <FileInput />
      </div>
    );
  }
}
ReactDOM.render(<App />, document.getElementById("root"));

const { Component, Fragment } = React;

function ListA() {
  return (
    <div>
      <div>aaaa</div>
      <div>bbbb</div>
      <div>cccc</div>
    </div>
  );
}

function ListB() {
  return (
    <Fragment>
      <div>dddd</div>
      <div>eeee</div>
      <div>ffff</div>
    </Fragment>
  );
}
class App extends Component {
  render() {
    return (
      <div>
        <div>start</div>
        <ListA />
        <ListB />
        <div>end</div>
      </div>
    );
  }
}
ReactDOM.render(<App />, document.getElementById("root"));

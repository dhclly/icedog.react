# icedog.react

React 练习项目，项目中有的序号不存在是因为当时想创建demo，但是后面给忘了写，就没做，于是删掉了

## 相关资源链接

- React官方 https://reactjs.org
- React 中国 https://zh-hans.reactjs.org
- React 文档 https://react.docschina.org
- React 入门实例教程 http://www.ruanyifeng.com/blog/2015/03/react.html
- React 入门教程Demo https://github.com/ruanyf/react-demos
- https://www.runoob.com/react


## React 练习要求

- react
- react-dom
- browser.js or babel.js

## 相关辅助工具

- react dev tool
- visual studio code
- vsc 插件 preview on borwser

## build目录说明

build 目录包含三个子目录

- babel-core
- react
- react-dom
- babel

通过链接可知
https://blog.csdn.net/wopelo/article/details/77844916

从Babel 6.0开始，不再直接提供浏览器版本，而是要用构建工具构建出来，这里可以通过安装老版本的babel-core模块来解决

```sh
npm install -g babel-core@old
```

这样子安装的版本应该是5.8.x，在node_modules会出现babel-core，里面就是关于babel的一切。 

babel 目录下的js来自阮一峰的练习项目,应该是新版的编译结果集

https://github.com/ruanyf/react-demos/tree/master/build

https://raw.githubusercontent.com/ruanyf/react-demos/master/build/babel.min.js

## JSX语法说明

- 可以在JS中书写XML（HTML）
- 只能有一个顶级元素
- 可以包含子节点
- 支持插值表达式
- class属性需要写成className
- style 需要传递一个对象 `<div style={ {color:"green" } }></div>`,外面的花括号是插值表达式，里面的表示对象
- 只支持插值表达式，不支持 for if 等语句
- 循环每个子项需要唯一key属性
- Object.keys(obj)来循环对象
- 组件通过标签属性传参
- 如果一个函数或类作为一个组件去使用的话，那么名称必须首字母大写
- 如果使用类实现组件，那么需要继承一个父类：React.Component
- 组件类必须实现一个render()的方法
- props：传入的参数必须是用对象下的一个属性props来接收


## React.Children

this.props.children 的值有三种可能：如果当前组件没有子节点，它就是 undefined ;如果有一个子节点，数据类型是 object ；如果有多个子节点，数据类型就是 array 。所以，处理 this.props.children 的时候要小心。

React 提供一个工具方法 React.Children 来处理 this.props.children 。我们可以用 React.Children.map 来遍历子节点，而不用担心 this.props.children 的数据类型是 undefined 还是 object。更多的 React.Children 的方法，请参考官方文档。

## PropTypes

https://reactjs.org/docs/components-and-props.html

## refs

https://reactjs.org/docs/refs-and-the-dom.html#callback-refs

## 组件的生命周期

componentWillMount 在渲染前调用,在客户端也在服务端。

componentDidMount : 在第一次渲染后调用，只在客户端。之后组件已经生成了对应的DOM结构，可以通过this.getDOMNode()来进行访问。 如果你想和其他JavaScript框架一起使用，可以在这个方法中调用setTimeout, setInterval或者发送AJAX请求等操作(防止异步操作阻塞UI)。

componentWillReceiveProps 在组件接收到一个新的 prop (更新后)时被调用。这个方法在初始化render时不会被调用。

shouldComponentUpdate 返回一个布尔值。在组件接收到新的props或者state时被调用。在初始化时或者使用forceUpdate时不被调用。 
可以在你确认不需要更新组件时使用。

componentWillUpdate在组件接收到新的props或者state但还没有render时被调用。在初始化时不会被调用。

componentDidUpdate 在组件完成更新后立即调用。在初始化时不会被调用。

componentWillUnmount在组件从 DOM 中移除之前立刻被调用。


在具有许多组件的应用程序中，在销毁时释放组件所占用的资源非常重要。

每当 Clock 组件第一次加载到 DOM 中的时候，我们都想生成定时器，这在 React 中被称为挂载。

同样，每当 Clock 生成的这个 DOM 被移除的时候，我们也会想要清除定时器，这在 React 中被称为卸载。


## CRA([create-react-app](https://github.com/facebookincubator/create-react-app))

> https://blog.csdn.net/zhengming0929/article/details/85256046

现在推荐这种方式快速搭建react app 开发环境

```bash
yarn create react-app antd-demo
```

## dangerouslySetInnerHTML

```javascript
function createMarkup() { return {__html: 'First  &middot; Second'}; };
    <div dangerouslySetInnerHTML={createMarkup()} />
```

Dangerously Set innerHTML - React 中文版 - 极客学院Wiki
http://wiki.jikexueyuan.com/project/react/dangerously.html
const { Component } = React;

class FilterableProductTable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filterText: "",
      inStockOnly: false
    };
  }
  handleFilterTextChange = filterText => {
    this.setState({
      filterText: filterText
    });
  };

  handleInStockChange = inStockOnly => {
    this.setState({
      inStockOnly: inStockOnly
    });
  };
  render() {
    const { filterText, inStockOnly } = this.state;
    const { products } = this.props;
    return (
      <div className="FilterableProductTable">
        <SearchBar
          {...{
            onFilterTextChange: this.handleFilterTextChange,
            onInStockChange: this.handleInStockChange,
            filterText,
            inStockOnly
          }}
        />
        <ProductTable
          {...{
            filterText,
            inStockOnly,
            products
          }}
        />
      </div>
    );
  }
}

class SearchBar extends Component {
  constructor(props) {
    super(props);
  }

  handleFilterTextChange = e => {
    const { onFilterTextChange } = this.props;
    onFilterTextChange(e.target.value);
  };

  handleInStockChange = e => {
    const { onInStockChange } = this.props;
    onInStockChange(e.target.checked);
  };

  render() {
    const { filterText, inStockOnly } = this.props;
    return (
      <form className="SearchBar">
        <input
          type="text"
          placeholder="Search..."
          value={filterText}
          onChange={this.handleFilterTextChange}
        />
        <p>
          <input
            type="checkbox"
            checked={inStockOnly}
            onChange={this.handleInStockChange}
          />{" "}
          Only show products in stock
        </p>
      </form>
    );
  }
}

function ProductTable(props) {
  const { filterText, inStockOnly, products } = props;

  const rows = [];
  let lastCategory = null;

  products.forEach(product => {
    //跳过找不到关键字的
    if (
      product.name
        .toLocaleLowerCase()
        .indexOf(filterText.toLocaleLowerCase()) === -1
    ) {
      return;
    }
    //跳过当只选中股票时不是股票的
    if (inStockOnly && !product.stocked) {
      return;
    }
    //找到类别
    if (product.category !== lastCategory) {
      rows.push(
        <ProductCategoryRow
          {...{
            category: product.category,
            key: product.category
          }}
        />
      );
    }
    rows.push(<ProductRow product={product} key={product.name} />);
    lastCategory = product.category;
  });

  return (
    <table className="ProductTable">
      <thead>
        <tr>
          <th>Name</th>
          <th>Price</th>
        </tr>
      </thead>
      <tbody>{rows}</tbody>
    </table>
  );
}

function ProductCategoryRow(props) {
  const { category } = props;
  return (
    <tr className="ProductCategoryRow">
      <th colSpan="2">{category}</th>
    </tr>
  );
}

function ProductRow(props) {
  const {
    product: { name, price, stocked }
  } = props;

  const newName = stocked ? (
    <span>
      {name}
      <span style={{ color: "red" }}>[stock]</span>
    </span>
  ) : (
    name
  );

  return (
    <tr className="ProductRow">
      <td>{newName}</td>
      <td>{price}</td>
    </tr>
  );
}

class App extends Component {
  state = {
    products: []
  };
  componentDidMount() {
    fetch("data.json", {
      method: "GET",
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(res => res.json())
      .then(data => {
        this.setState({ products: data });
      });
  }
  render() {
    return (
      <div>
        <FilterableProductTable products={this.state.products} />
      </div>
    );
  }
}
ReactDOM.render(<App />, document.getElementById("root"));

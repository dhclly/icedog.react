const { Component } = React;

function FancyBorder(props) {
  return (
    <div
      className={`FancyBorder ${
        props.color ? "FancyBorder-" + props.color : ""
      }`}
    >
      {props.children}
    </div>
  );
}

function WelcomeDialog() {
  return (
    <div>
      <FancyBorder color="blue">
        <h1 className="Dialog-title">Welcome</h1>
        <p className="Dialog-message">Thank you for visiting our spacecraft!</p>
      </FancyBorder>
      <FancyBorder>
        <h1 className="Dialog-title">Welcome</h1>
        <p className="Dialog-message">Thank you for visiting our spacecraft!</p>
      </FancyBorder>
    </div>
  );
}

function SplitPane(props) {
  return (
    <div className="SplitPane">
      <div className="SplitPane-left">left:{props.left}</div>
      <div className="SplitPane-right">right:{props.right}</div>
    </div>
  );
}
function CCPanel() {
  return <SplitPane left={<WelcomeDialog />} right={<WelcomeDialog />} />;
}

function BaseDialog(props) {
  return (
    <FancyBorder color="blue">
      <h1 className="Dialog-title">{props.title}</h1>
      <p className="Dialog-message">{props.message}</p>
    </FancyBorder>
  );
}

function GreetDialog() {
  return (
    <BaseDialog
      title="Welcome"
      message="Thank you for visiting our spacecraft!"
    />
  );
}
class App extends Component {
  render() {
    return (
      <div>
        <WelcomeDialog />
        <CCPanel />
        <GreetDialog />
      </div>
    );
  }
}
ReactDOM.render(<App />, document.getElementById("root"));


## 井字棋游戏

入门教程: 认识 React – React
https://zh-hans.reactjs.org/tutorial/tutorial.html


Hello World – React
https://zh-hans.reactjs.org/docs/hello-world.html

```
yarn create react-app tic-tac-toe
```

在游戏历史记录列表显示每一步棋的坐标，格式为 (列号, 行号)。
在历史记录列表中加粗显示当前选择的项目。
使用两个循环来渲染出棋盘的格子，而不是在代码里写死（hardcode）。
添加一个可以升序或降序显示历史记录的按钮。
每当有人获胜时，高亮显示连成一线的 3 颗棋子。
当无人获胜时，显示一个平局的消息


安装 serve服务

```
cnpm install -g serve
yarn global add serve
```

```
serve -s build #运行服务
```

## 项目地址：

https://gitee.com/dhclly/icedog.react/tree/master/20-tic-tac-toe
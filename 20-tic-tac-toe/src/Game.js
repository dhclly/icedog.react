import React, { Component } from "react";
import Board from "./Board";
import "./Game.css";

class Game extends Component {
  constructor(props) {
    super(props);
    this.state = {
      history: [
        {
          squares: Array(9).fill(null),
          x: 0,
          y: 0,
          index: 0
        }
      ],
      xIsNext: true,
      stepNumber: 0,
      recordSortDesc: false,
    };
  }
  handleClick = i => {
    //调用了 .slice() 方法创建了 squares 数组的一个副本，
    //而不是直接在现有的数组上进行修改,这样方便数据的历史回溯
    const { calculateWinner } = this;
    const { history } = this.state;
    const newHistory = history.slice(0, this.state.stepNumber + 1);
    const current = newHistory[newHistory.length - 1];
    const squares = current.squares.slice();

    if (calculateWinner(squares).name !== null || squares[i] !== null) {
      return;
    }

    squares[i] = this.state.xIsNext ? "X" : "O";
    this.setState({
      history: newHistory.concat([
        {
          squares: squares,
          x: (i % 3) + 1,
          y: Math.floor(i / 3) + 1,
          index: newHistory.length
        }
      ]),
      stepNumber: newHistory.length,
      xIsNext: !this.state.xIsNext
    });
  };

  calculateWinner = squares => {
    //胜利的几种情况
    // 0 1 2
    // 3 4 5
    // 6 7 8
    const lines = [
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8],
      [2, 4, 6]
    ];
    for (let i = 0; i < lines.length; i++) {
      const [a, b, c] = lines[i];
      //abc三个值均为X 或者 O 胜利
      if (
        squares[a] !== null &&
        squares[a] === squares[b] &&
        squares[a] === squares[c]
      ) {
        return {name:squares[a],winnerLine:lines[i]};
      }
    }
    return {name:null,winnerLine:[]};
  };

  jumpTo = step => {
    this.setState({
      stepNumber: step,
      xIsNext: step % 2 === 0 //X先行
    });
  };

  sortRecord = recordSortDesc => {
    this.setState({
      recordSortDesc: !this.state.recordSortDesc
    });
  };

  render() {
    const { calculateWinner, handleClick, jumpTo } = this;
    const { history, stepNumber, recordSortDesc } = this.state;
    const current = history[this.state.stepNumber];
    const winner = calculateWinner(current.squares);

    let steps = history.map((current, step) => {
      const desc =
        current.index > 0 ? "跳到第" + current.index + "步" : "跳到游戏起点";
      return (
        <li
          key={current.index}
          className={`item ${current.index === stepNumber ? "active" : ""}`}
          onClick={() => jumpTo(current.index)}
        >
          <span className="text">
            {current.index+1}-{desc}
          </span>
          <span>{`[X:${current.x},Y:${current.y}]`}</span>
        </li>
      );
    });
    if (this.state.recordSortDesc) {
      steps = steps.reverse();
    }
    let status;
    if (winner.name !== null) {
      status = `胜利玩家: ${winner.name}`;
    } else if(this.state.stepNumber===9){
      status =`平局!`
    }
    else {
      status = "轮到玩家: " + (this.state.xIsNext ? "X" : "O");
    }
    const boardProps = {
      squares: current.squares,
      winnerLine:winner.winnerLine,
      onClick: i => handleClick(i),
    };
    return (
      <div className="game">
        <div className="game-board">
        <div className="game-status">{status}</div>
          <Board {...boardProps} />
        </div>
        <div className="game-info">
          <button onClick={() => this.sortRecord(recordSortDesc)}>
            {recordSortDesc ? "降序排序" : "升序排序"}
          </button>
          <ul className="step-list">{steps}</ul>
        </div>
      </div>
    );
  }
}

export default Game;

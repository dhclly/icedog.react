import React, { Component } from "react";
import Square from "./Square";
import "./Board.css";

class Board extends Component {

  renderSquare(i) {
    const { squares, onClick,winnerLine} = this.props;

    const props = {
      value: squares[i],
      isWinnerLine:winnerLine.includes(i),
      onClick: () => onClick(i), //onClick是传递给子级组件的监听事件，handleClick是onClick触发后的处理函数
    };
    return <Square key={i} {...props} />;
  }

  render() {
    const boardGrid = [];
    for (let a = 0; a < 3; a++) {
      let children = [];
      for (let b = 0; b < 3; b++) {
        var col = this.renderSquare(3 * a + b);
        children.push(col);
      }
      let row = <div key={a} className="board-row">{children}</div>;
      boardGrid.push(row);
    }
    return (
      <div>
        {boardGrid}
      </div>
    );
  }
}

export default Board;

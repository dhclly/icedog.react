import React from "react";
import logo from "./logo.svg";
import Game from "./Game";
import "./App.css";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <span>React 井字棋</span>
        <span> - </span>
        <a href="https://zh-hans.reactjs.org/tutorial/tutorial.html" target="_new" style={{color:'orange',cursor:'pointer'}}>[教程链接]</a>
        <div class="App-desc">
          <div class="desc1">
            在游戏历史记录列表显示每一步棋的坐标，格式为 (列号, 行号)。
            在历史记录列表中加粗显示当前选择的项目。
            使用两个循环来渲染出棋盘的格子，而不是在代码里写死（hardcode）。
            添加一个可以升序或降序显示历史记录的按钮。
            每当有人获胜时，高亮显示连成一线的 3 颗棋子。
            当无人获胜时，显示一个平局的消息
          </div>
          <div class="desc2">获胜条件，3个格子连成一线</div>
        </div>
      </header>
      <div className="App-body">
        <Game />
      </div>
    </div>
  );
}

export default App;

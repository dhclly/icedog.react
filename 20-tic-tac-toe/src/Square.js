import React /*, { Component }*/ from "react";
import "./Square.css";


// class Square extends Component {
//   render() {
//     const { value, onClick } = this.props;
//     const btnProps = {
//       className: "square",
//       onClick, //父级传过来的onClick监听函数
//     };
//     return <button {...btnProps}>{value}</button>;
//   }
// }


function Square(props) {
  const { value, onClick,isWinnerLine } = props;
  const btnProps = {
    className: `square ${value==='X'?'black':'red'} ${isWinnerLine?'winner-line':''}`,
    onClick //父级传过来的onClick监听函数
  };
  return <button {...btnProps}>{value}</button>;
}

export default Square;

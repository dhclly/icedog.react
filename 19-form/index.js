const { Component } = React;

class HelloMessage extends React.Component {
  constructor(props) {
    super(props);
    this.state = { value: "Hello Runoob!" };
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    this.setState({ value: event.target.value });
  }
  render() {
    var value = this.state.value;
    return (
      <div>
        <input type="text" value={value} onChange={this.handleChange} />
        <h4>{value}</h4>
      </div>
    );
  }
}

class TextContent extends React.Component {
  render() {
    const { value, handleChange } = this.props;
    return (
      <div>
        <input type="text" value={value} onChange={handleChange} />
        <h4>{value}</h4>
      </div>
    );
  }
}

class HiMessage extends React.Component {
  constructor(props) {
    super(props);
    this.state = { value: "Hello Runoob!" };
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    this.setState({ value: event.target.value });
  }
  render() {
    const { value } = this.state;
    const props = {
      value,
      handleChange: this.handleChange
    };
    return (
      <div>
        <span>嵌套子组件</span>
        <TextContent {...props} />
        <div>Parent:{this.state.value}</div>
      </div>
    );
  }
}

class FlavorForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = { value: "Taobao" };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({ value: event.target.value });
  }

  handleSubmit(event) {
    alert("Your favorite flavor is: " + this.state.value);
    event.preventDefault();
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <label>
          选择您最喜欢的网站
          <select value={this.state.value} onChange={this.handleChange}>
            <option value="Google">Google</option>
            <option value="Runoob">Runoob</option>
            <option value="Taobao">Taobao</option>
            <option value="Facebook">Facebook</option>
          </select>
        </label>
        <input type="submit" value="提交" />
      </form>
    );
  }
}

class Reservation extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isGoing: true,
      numberOfGuests: 2
    };

    this.handleInputChange = this.handleInputChange.bind(this);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.type === "checkbox" ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  render() {
    return (
      <form>
        <label>
          是否离开:
          <input
            name="isGoing"
            type="checkbox"
            checked={this.state.isGoing}
            onChange={this.handleInputChange}
          />
          <span>{this.state.isGoing.toString()}</span>
        </label>
        <br />
        <label>
          访客数:
          <input
            name="numberOfGuests"
            type="number"
            value={this.state.numberOfGuests}
            onChange={this.handleInputChange}
          />
          <span>{this.state.numberOfGuests.toString()}</span>
        </label>
      </form>
    );
  }
}

class App extends Component {
  render() {
    return (
      <div>
        <HelloMessage />
        <HiMessage />
        <FlavorForm />
        <Reservation />
      </div>
    );
  }
}

ReactDOM.render(<App />, document.getElementById("root"));

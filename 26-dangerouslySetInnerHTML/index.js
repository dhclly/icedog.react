const { Component } = React;

class App extends Component {
  getDangerousText = () => {
    return {
      __html: "<div>img</div><img onerror=\"(function(){alert('you died');alert('you died');alert('you died')})()\" src=\"/b.png\"/>"
    };
  };
  render() {
    return <div dangerouslySetInnerHTML={this.getDangerousText()}></div>;
  }
}
ReactDOM.render(<App />, document.getElementById("root"));

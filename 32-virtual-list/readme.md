一种实现 virtual list 的方式 | Ayou
http://www.paradeto.com/2018/10/25/react-virtual-list/


```
ref={ref => (this.$container = ref)}
```


整体思路是通过holder在container里面进行撑开，然后形成滚动，在滚动的时候，获取container的 scrollTop，然后通过这个值进行计算要显示项的数据，同时让实际显示的数据列表的位置处于可视区。
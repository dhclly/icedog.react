This demo is copied from [github.com/mhart/react-server-example](https://github.com/mhart/react-server-example), but I rewrote it with JSX syntax.

```bash
# install the dependencies in demo13 directory
$ npm install

# translate all jsx file in src subdirectory to js file
$ npm run build

# launch http server
$ node server.js
```

关于ES2015转换es5遇到的‘this’变成‘undefined’问题查找记录 

https://www.jianshu.com/p/a182d4f75e88

https://www.npmjs.com/package/literalify